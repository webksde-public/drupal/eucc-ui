# Setup enviroment

Use 'npm install', 'gulp build' and 'gulp' (default task) to start watching SASS files.

# Icons

For the demo we've used the GPL iconset "iconic", see: https://useiconic.com/icons/
We've added it as iconfont for now, better use SVG in production.
