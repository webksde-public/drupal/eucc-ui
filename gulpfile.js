var gulp = require("gulp"),
	  sass = require("gulp-sass"),
	  postcss = require("gulp-postcss"),
	  autoprefixer = require("autoprefixer"),
    sourcemaps = require("gulp-sourcemaps"),
    npmDist = require('gulp-npm-dist');

// SASS => CSS
function style() {
  return (
      gulp
          .src("sass/*.scss")
          // Initialize sourcemaps before compilation starts
          .pipe(sourcemaps.init())
          .pipe(sass())
          .on("error", sass.logError)
          // Use postcss with autoprefixer and compress the compiled file using cssnano
          .pipe(postcss([autoprefixer()]))
          // Now add/write the sourcemaps
          .pipe(sourcemaps.write())
          .pipe(gulp.dest("css"))
  );
}

// Move frontend libraries from node_modules to libs folder
function moveFrontendLibraries() {
  return (
    gulp.src(npmDist(), {base:'./node_modules/'})
        // .pipe(rename(function(path) {
        //     path.dirname = path.dirname.replace(/\/dist/, '').replace(/\\dist/, '');
        // }))
        .pipe(gulp.dest('./libs'))
  );
}

exports.build = gulp.series(style, moveFrontendLibraries);
exports.default = function () {
  // You can use a single task
  gulp.watch("sass/*.scss", style);
};
