// Demo script: Toggle banner visibility
document.addEventListener("DOMContentLoaded", function(){
  var euccWrapper = document.querySelector('[data-eucc]');
  var euccToggleButtons = document.querySelectorAll('[data-eucc-toggle]');

  for (var i = 0; i < euccToggleButtons.length; i++) {
    euccToggleButtons[i].addEventListener('click', function(event) {
      euccWrapper.classList.toggle('eucc--visible');
    });
  }
});
